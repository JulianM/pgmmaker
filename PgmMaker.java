import java.io.*;
import java.util.ArrayList;

/**
 * Created by Julian on 23.02.2015.
 */
public class PgmMaker {

    public static String fileName;
    public static int width, height;

    public static void main(String[] args) {
        fileName = args[0];
        width = Integer.parseInt(args[1]);
        height = Integer.parseInt(args[2]);

        ArrayList<Integer> pixels = new ArrayList<Integer>();

        try {
            PrintWriter writer = new PrintWriter(fileName, "ASCII");

            // magic number
            writer.println("P2");
            // size
            writer.println(width + " " + height);
           // number of grayscales
            writer.println("16");

            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            String s;
            while ((s = in.readLine()) != null && s.length() != 0) {
                String[] t = s.split(" ");
                String u = t[t.length - 1];

                /*pixels.add((Integer.parseInt(u.substring(6, 8), 16) & 60) >> 2);
                pixels.add((Integer.parseInt(u.substring(4, 6), 16) & 60) >> 2);
                pixels.add((Integer.parseInt(u.substring(2, 4), 16) & 60) >> 2);
                pixels.add((Integer.parseInt(u.substring(0, 2), 16) & 60) >> 2);*/

                pixels.add((Integer.parseInt(u.substring(0, 2), 16) & 60) >> 2);
                pixels.add((Integer.parseInt(u.substring(2, 4), 16) & 60) >> 2);
                pixels.add((Integer.parseInt(u.substring(4, 6), 16) & 60) >> 2);
                pixels.add((Integer.parseInt(u.substring(6, 8), 16) & 60) >> 2);
            }

            // pixels

            for (int i = 0; i < pixels.size(); i++) {
                final int b = pixels.get(i);
                if (b < 10) {
                    writer.print("   ");
                } else if (b < 100) {
                    writer.print("  ");
                } else {
                    writer.print(" ");
                }
                writer.print(b);

                if ((i + 1) % width == 0) {
                    writer.println();
                }
            }

            writer.println();

            writer.close();
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

}
